import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageGateway } from './message.gateway';
import { MongooseModule } from '@nestjs/mongoose';
import { Message, MessageSchema } from './shemas/message.shemas';

@Module({
  imports: [MessageModule,  MongooseModule.forFeature([
    { name: Message.name, schema: MessageSchema },
  ]),],
  providers: [MessageGateway, MessageService],
  exports: [MessageService]
})
export class MessageModule {} 
 