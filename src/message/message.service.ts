import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Cour, CourDocument } from 'src/cours/shemas/cours.shemas';
import { User, UserDocument } from 'src/users/shemas/user.shemas';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Message, MessageDocument } from './shemas/message.shemas';

@Injectable()
export class MessageService {
  constructor(
    @InjectModel(Message.name) private messageModel: Model<MessageDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Cour.name) private courModel: Model<CourDocument>,

  ) {}

  async createMessage(content: string, sender: string, receiver: string, cour: string): Promise<Message> {
    const senders = await this.userModel.findById(sender);
    const recipient = await this.userModel.findById(receiver);
    const course = await this.courModel.findById(cour);

    const newMessage = new this.messageModel({ content, sender, recipient, course });
    return newMessage.save();
  }

  async getMessagesByUser(userId: string): Promise<Message[]> {
    return this.messageModel.find({ $or: [{ 'sender._id': userId }, { 'recipient._id': userId }] }).exec();
  }

  // async createMessage(
  //   content: string,
  //   sender: string,
  //   receiver: string,
  //   cours: string,
  // ): Promise<Message> {
  //   const newMessage = new this.messageModel({
  //     content,
  //     sender,
  //     receiver,
  //     cours,
  //   });
  //   return newMessage.save();
  // }

  // async getMessages(
  //   sender: string,
  //   receiver: string,
  //   cours: string,
  // ): Promise<Message[]> {
  //   return this.messageModel
  //     .find({ sender, receiver, cours })
  //     .sort({ createdAt: 'asc' })
  //     .exec();
  // }

  // async deleteMessage(id: string): Promise<Message> {
  //   return this.messageModel.findByIdAndDelete(id).exec();
  // }

  // create(createMessageDto: CreateMessageDto) {
  //   return 'This action adds a new message';
  // }

  // findAll() {
  //   return `This action returns all message`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} message`;
  // }

  // update(id: number, updateMessageDto: UpdateMessageDto) {
  //   return `This action updates a #${id} message`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} message`;
  // }


































 














































//   le code ca dans le message.service.ts
// import { Injectable } from '@nestjs/common';
// import { InjectModel } from '@nestjs/mongoose';
// import { Model } from 'mongoose';
// import { CreateMessageDto } from './dto/create-message.dto';
// import { UpdateMessageDto } from './dto/update-message.dto';
// import { Message, MessageDocument } from './shemas/message.shemas';

// @Injectable()
// export class MessageService {
//   constructor(
//     @InjectModel(Message.name) private messageModel: Model<MessageDocument>,
//   ) {}

//   async createMessage(
//     content: string,
//     sender: string,
//     receiver: string,
//     cours: string,
//   ): Promise<Message> {
//     const newMessage = new this.messageModel({
//       content,
//       sender,
//       receiver,
//       cours,
//     });
//     return newMessage.save();
//   }

//   async getMessages(
//     sender: string,
//     receiver: string,
//     cours: string,
//   ): Promise<Message[]> {
//     return this.messageModel
//       .find({ sender, receiver, cours })
//       .sort({ createdAt: 'asc' })
//       .exec();
//   }

//   async deleteMessage(id: string): Promise<Message> {
//     return this.messageModel.findByIdAndDelete(id).exec();
//   }
// et le code ca dans le message.gateway.ts
// import {
//   WebSocketGateway,
//   SubscribeMessage,
//   MessageBody,
//   WebSocketServer,
// } from '@nestjs/websockets';
// import { MessageService } from './message.service';
// import { CreateMessageDto } from './dto/create-message.dto';
// import { UpdateMessageDto } from './dto/update-message.dto';
// import { Server, Socket } from 'socket.io';

// @WebSocketGateway()
// export class MessageGateway {
//   constructor(private readonly messageService: MessageService) {}

//   @WebSocketServer()
//   server: Server; 

//   @SubscribeMessage('joinRoom')
//   handleJoinRoom(
//     client: Socket,
//     data: { senderId: string; receiverId: string; coursId: string },
//   ) {
//     client.join(data.senderId + data.receiverId + data.coursId);
//   }

//   @SubscribeMessage('leaveRoom')
//   handleLeaveRoom(
//     client: Socket,
//     data: { senderId: string; receiverId: string; coursId: string },
//   ) {
//     client.leave(data.senderId + data.receiverId + data.coursId);
//   }

//   @SubscribeMessage('sendMessage')
//   async handleMessage(
//     client: Socket,
//     data: {
//       content: string;
//       senderId: string;
//       receiverId: string;
//       coursId: string;
//     },
//   ) {
//     const { content, senderId, receiverId, coursId } = data;

//     // Create a new message
//     const message = await this.messageService.createMessage(
//       content,
//       senderId,
//       receiverId,
//       coursId,
//     );

//     // Send the message to the sender and receiver
//     const senderSocket = this.server.sockets.sockets.get(client.id);
//     senderSocket.emit('message', message);
//     client.to(senderId + receiverId + coursId).emit('message', message);
//   }

//   @SubscribeMessage('getMessages')
//   async handleGetMessages(
//     client: Socket,
//     data: { senderId: string; receiverId: string; coursId: string },
//   ) {
//     const { senderId, receiverId, coursId } = data;

//     // Retrieve the messages from the database
//     const messages = await this.messageService.getMessages(
//       senderId,
//       receiverId,
//       coursId,
//     );

//     // Send the messages to the client
//     client.emit('messages', messages);
//   }

//   @SubscribeMessage('deleteMessage')
//   async handleDeleteMessage(client: Socket, id: string) {
//     // Delete the message from the database
//     const deletedMessage = await this.messageService.deleteMessage(id);

//     // Send the deleted message to the client
//     client.emit('messageDeleted', deletedMessage);
//   }
// et le code ca dans le fichier message.module.ts
// import { Module } from '@nestjs/common';
// import { MessageService } from './message.service';
// import { MessageGateway } from './message.gateway';
// import { MongooseModule } from '@nestjs/mongoose';
// import { Message, MessageSchema } from './shemas/message.shemas';

// @Module({
//   imports: [MessageModule,  MongooseModule.forFeature([
//     { name: Message.name, schema: MessageSchema },
//   ]),],
//   providers: [MessageGateway, MessageService],
//   exports: [MessageService]
// })
// export class MessageModule {} 
 
// maintenant verifier cette code et comment tester cette code dans le postman ou bien dans le navigateur
// }
}