import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import { CourSchema } from 'src/cours/shemas/cours.shemas';
import { UserSchema } from 'src/users/shemas/user.shemas';

export type MessageDocument = Message & Document;

@Schema()
export class Message {
  @Prop()
  content: string;

  //   @Prop(UserSchema)
  //   sender: string;

  //   @Prop(UserSchema)
  //   receiver: string;

  //   @Prop(CourSchema)
  //   cours: string;
  @Prop()
  sender: [{ type: ObjectId; ref: 'User' }];

  @Prop()
  receiver: [{ type: ObjectId; ref: 'User' }];

  @Prop()
  cours: [{ type: ObjectId; ref: 'Cour' }];

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const MessageSchema = SchemaFactory.createForClass(Message);
