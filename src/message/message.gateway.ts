import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  WebSocketServer,
} from '@nestjs/websockets';
import { MessageService } from './message.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { UpdateMessageDto } from './dto/update-message.dto';
import { Server, Socket } from 'socket.io';

@WebSocketGateway()
export class MessageGateway {
  constructor(private readonly messageService: MessageService) {}

  @WebSocketServer()
  server: Server;

  afterInit(server: any) {
    console.log('Socket server initialized');
  }

  handleConnection(client: Socket, ...args: any[]) {
    console.log(`Client connected: ${client.id}`);
  }

  handleDisconnect(client: Socket) {
    console.log(`Client disconnected: ${client.id}`);
  }

  @SubscribeMessage('message')
  async handleMessage(client: Socket, payload: any) {
    const { content, senderId, recipientId, courseId } = payload;
    const newMessage = await this.messageService.createMessage(content, senderId, recipientId, courseId);

    client.broadcast.emit('message', newMessage);
  }

  @SubscribeMessage('getMessages')
  async handleGetMessages(client: Socket, payload: any) {
    const { userId } = payload;
    const messages = await this.messageService.getMessagesByUser(userId);

    client.emit('messages', messages);
  }

  // @WebSocketServer()
  // server: Server; 

  // @SubscribeMessage('joinRoom')
  // handleJoinRoom(
  //   client: Socket,
  //   data: { senderId: string; receiverId: string; coursId: string },
  // ) {
  //   client.join(data.senderId + data.receiverId + data.coursId);
  // }

  // @SubscribeMessage('leaveRoom')
  // handleLeaveRoom(
  //   client: Socket,
  //   data: { senderId: string; receiverId: string; coursId: string },
  // ) {
  //   client.leave(data.senderId + data.receiverId + data.coursId);
  // }

  // @SubscribeMessage('sendMessage')
  // async handleMessage(
  //   client: Socket,
  //   data: {
  //     content: string;
  //     senderId: string;
  //     receiverId: string;
  //     coursId: string;
  //   },
  // ) {
  //   const { content, senderId, receiverId, coursId } = data;

  //   // Create a new message
  //   const message = await this.messageService.createMessage(
  //     content,
  //     senderId,
  //     receiverId,
  //     coursId,
  //   );

  //   // Send the message to the sender and receiver
  //   const senderSocket = this.server.sockets.sockets.get(client.id);
  //   senderSocket.emit('message', message);
  //   client.to(senderId + receiverId + coursId).emit('message', message);
  // }

  // @SubscribeMessage('getMessages')
  // async handleGetMessages(
  //   client: Socket,
  //   data: { senderId: string; receiverId: string; coursId: string },
  // ) {
  //   const { senderId, receiverId, coursId } = data;

  //   // Retrieve the messages from the database
  //   const messages = await this.messageService.getMessages(
  //     senderId,
  //     receiverId,
  //     coursId,
  //   );

  //   // Send the messages to the client
  //   client.emit('messages', messages);
  // }

  // @SubscribeMessage('deleteMessage')
  // async handleDeleteMessage(client: Socket, id: string) {
  //   // Delete the message from the database
  //   const deletedMessage = await this.messageService.deleteMessage(id);

  //   // Send the deleted message to the client
  //   client.emit('messageDeleted', deletedMessage);
  // }

  // @SubscribeMessage('createMessage')
  // create(@MessageBody() createMessageDto: CreateMessageDto) {
  //   return this.messageService.create(createMessageDto);
  // }

  // @SubscribeMessage('findAllMessage')
  // findAll() {
  //   return this.messageService.findAll();
  // }

  // @SubscribeMessage('findOneMessage')
  // findOne(@MessageBody() id: number) {
  //   return this.messageService.findOne(id);
  // }

  // @SubscribeMessage('updateMessage')
  // update(@MessageBody() updateMessageDto: UpdateMessageDto) {
  //   return this.messageService.update(updateMessageDto.id, updateMessageDto);
  // }

  // @SubscribeMessage('removeMessage')
  // remove(@MessageBody() id: number) {
  //   return this.messageService.remove(id);
  // }
}
