import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateFormationDto } from './dto/create-formation.dto';
import { UpdateFormationDto } from './dto/update-formation.dto';
import { Fiormation, FormationDocument } from './shemas/formation.shemas';

@Injectable()
export class FormationService {
  constructor(
    @InjectModel(Fiormation.name)
    private formationModel: Model<FormationDocument>,
  ) {}

  async create(createFormationDto: CreateFormationDto): Promise<Fiormation> {
    const newformation = await new this.formationModel(createFormationDto);
    console.log('new formation', newformation);
    // return (await newformation.save()).populate({ path: "id_formateur", model: "User" });
    return newformation.save();
  }

  async findAll() /*: Promise<Cour[]>*/ {
    const listformation = await this.formationModel.find().exec();
    if (!listformation || listformation.length == 0) {
      throw new NotFoundException('Données des formations non trouvées !');
    }
    return { total: listformation.length, listformation };
  }
  // (/*........*/ )
  // async findAll(page: number = 1, limit: number = 10): Promise<{ total: number, formation: Fiormation[] }> {
  //   const skip = (page - 1) * limit;
  //   const [count, formation] = await Promise.all([
  //     this.formationModel.countDocuments(),
  //     this.formationModel
  //       .find()
  //       .populate({ path: "admin", model: "User" })
  //       .skip(skip)
  //       .limit(limit)
  //       .exec(),
  //   ]);
  //   if (formation.length == 0) {
  //     throw new NotFoundException('Données des cours non trouvées !');
  //   }
  //   return { total: count, formation };
  // }
  // async findAll(page: number = 1, limit: number ,/* search: string = ''*/ ): Promise<{ total: number, formation: Fiormation[] }> {
  //   const skip = (page - 1) * limit;
  //   // const query = search ? { titre: { $regex: search, $options: 'i' } } : {};

  //   const [count, formation] = await Promise.all([
  //     this.formationModel.countDocuments(/*query*/),
  //     this.formationModel
  //       .find(/*query*/)
  //       .populate({ path: "admin", model: "User" })
  //       .skip(skip)
  //       .limit(limit)
  //       .exec(),
  //   ]);
  //   if (formation.length == 0) {
  //     throw new NotFoundException('Données des cours non trouvées !');
  //   }
  //   return { total: count, formation };
  // }

  async findOne(formationId: string): Promise<Fiormation> {
    const existingformation = await this.formationModel
      .findById(formationId) /*.populate({path: "id_formateur", model:"User"})*/
      .exec();
    if (!existingformation) {
      throw new NotFoundException(`formation #${formationId} not found`);
    }
    return existingformation;
  }

  async update(
    formationId: string,
    updateformationDto: UpdateFormationDto,
  ): Promise<Fiormation> {
    const existingformation = await this.formationModel.findByIdAndUpdate(
      formationId,
      updateformationDto,
      { new: true },
    );
    if (!existingformation) {
      throw new NotFoundException(`formation #${formationId} not found`);
    }
    return existingformation;
  }

  async remove(formationId: string): Promise<Fiormation> {
    const deleteformation = await this.formationModel.findByIdAndDelete(
      formationId,
    );
    if (!deleteformation) {
      throw new NotFoundException(`formation #${formationId} not found`);
    }
    return deleteformation;
  }
}
