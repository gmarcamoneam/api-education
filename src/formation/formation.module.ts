import { Module } from '@nestjs/common';
import { FormationService } from './formation.service';
import { FormationController } from './formation.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Fiormation, FormationSchema } from './shemas/formation.shemas';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Fiormation.name, schema: FormationSchema },
    ]),
  ],

  controllers: [FormationController],
  providers: [FormationService],
  exports: [FormationService],
})
export class FormationModule {}
