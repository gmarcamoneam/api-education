import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { CourSchema } from 'src/cours/shemas/cours.shemas';
import { UserSchema } from 'src/users/shemas/user.shemas';

export type FormationDocument = Fiormation & Document;

@Schema({ timestamps: true })
export class Fiormation {
  @Prop()
  type: string;

  @Prop()
  dureer: string;
  @Prop()
  prix: string;
  @Prop()
  heure: string;

  @Prop(UserSchema)
  id_formateur: string;

  @Prop(UserSchema)
  id_etudiant: string;

  @Prop(UserSchema)
  admin: string;

  @Prop(CourSchema)
  id_cour: string;
}

export const FormationSchema = SchemaFactory.createForClass(Fiormation);
