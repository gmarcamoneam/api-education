import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Put,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { FormationService } from './formation.service';
import { CreateFormationDto } from './dto/create-formation.dto';
import { UpdateFormationDto } from './dto/update-formation.dto';
import { AuthGuard } from '@nestjs/passport';
import { Fiormation } from './shemas/formation.shemas';

@Controller('formation')
export class FormationController {
  constructor(private readonly formationService: FormationService) {}

  @UseGuards(AuthGuard('admin')) 
  @Post('create')
 async create(@Body() createFormationDto: CreateFormationDto, @Res()response ) {
    // return this.formationService.create(createFormationDto);
    try {
      const formation = await this.formationService.create(createFormationDto)
      return response.status(HttpStatus.CREATED).json({
        status:HttpStatus.OK,
        message:"formation created successufly",
        data:formation
      }) 
    } catch (error) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        status:HttpStatus.BAD_REQUEST,
        message:" failed to create formation",
        data:null})
    }
  }

  @Get('getall')
 async findAll(@Res() response) {
    // return this.formationService.findAll();
    try {
      const listformations = await this.formationService.findAll();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des formation ont été trouvées avec succès',
        data: listformations,
      });
    } catch (err) {
      response.status(400).send({ message: err.message });
    }
  }

  @Get('getbyid/:id')
  findOne(@Param('id') id: string) {
    return this.formationService.findOne(id);
  }

  // @UseGuards(AuthGuard('admin'))
  // @Patch('update/:id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateFormationDto: UpdateFormationDto,
  // ) {
  //   return this.formationService.update(id, updateFormationDto);
  // }
  @UseGuards(AuthGuard('admin'))         
  @Put('update/:id')  
  update(    
    @Param('id') id: string,
    @Body() updateFormationDto: UpdateFormationDto,
  ) {
    return this.formationService.update(id, updateFormationDto);
  }

  @UseGuards(AuthGuard('admin'))  
  @Delete('delete/:id')
  remove(@Param('id') id: string) {
    return this.formationService.remove(id);
  }
}
