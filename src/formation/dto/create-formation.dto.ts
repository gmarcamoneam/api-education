import { ObjectId } from 'mongoose';

export class CreateFormationDto {
  type: string;
  dureer: string;
  prix:string
  heure:string
  id_formateur: { type: ObjectId; ref: 'User' };
  id_etudiant: { type: ObjectId; ref: 'User' };
  id_cour: { type: ObjectId; ref: 'Cour' };
}
