import { ObjectId } from "mongoose";

export class CreateTestDto {
    durer: string;
    description: string;
    // formateur: { type: ObjectId; ref: 'User' };
    readonly formateur: {
        _id: ObjectId;
        firstname: string;
        lastname: string;
        email: string;
        address: string;
        phone: string;
        picture: string;
        domaine:string
      };
    etudiant: { type: ObjectId; ref: 'User' };
}
