import { Module } from '@nestjs/common';
import { TestService } from './test.service';
import { TestController } from './test.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from 'src/users/shemas/user.shemas';
import { UsersService } from 'src/users/users.service';
import { Test, TestSchema } from './shemas/tests.shemas';
import { Cour, CourSchema } from 'src/cours/shemas/cours.shemas';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Test.name, schema: TestSchema }]),
  MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  MongooseModule.forFeature([{ name: Cour.name, schema: CourSchema }]),


  ], 

  controllers: [TestController],
  providers: [TestService,UsersService],
  exports: [TestService]
})
export class TestModule {}
