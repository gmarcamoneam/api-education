import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { TestService } from './test.service';
import { CreateTestDto } from './dto/create-test.dto';
import { UpdateTestDto } from './dto/update-test.dto';
import { UseGuards } from '@nestjs/common/decorators';
import { AuthGuard } from '@nestjs/passport';

@Controller('test')
export class TestController {
  constructor(private readonly testService: TestService) {}

  @UseGuards(AuthGuard('formateur'))
  @Post('create')
  async createoffice(@Res() response, @Body() createTestDto: CreateTestDto) {
    try {
      const newtest = await this.testService.create(createTestDto);
      return response.status(HttpStatus.CREATED).json({
        message: 'test a été créé avec succès',
        data: newtest,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Erreur : test non créé !',
        erreur: 'Bad Request',
      });
    }
  }
  @Get('getall')
  async getoffices(@Res() response) {
    try {
      const listtest = await this.testService.findAll();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des tests ont été trouvées avec succès',
        data: listtest,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Get('getbyid/:id')
  async getofficebyid(@Res() response, @Param('id') testId: number) {
    try {
      const existingtest = await this.testService.findOne(testId);
      return response.status(HttpStatus.OK).json({
        message: 'test trouvé avec succès',
        data: existingtest,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @UseGuards(AuthGuard('formateur'))
  @Patch('update/:id')
  async updateStudent(
    @Res() response,
    @Param('id') testid: number,
    @Body() updateTestDto: UpdateTestDto,
  ) {
    try {
      const existingtest = await this.testService.update(testid, updateTestDto);
      return response.status(HttpStatus.OK).json({
        message: 'le test a été mis à jour avec succès',
        data: existingtest,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @UseGuards(AuthGuard('formateur'))
  @Delete('delete/:id')
  async deleteoffice(@Res() response, @Param('id') testid: number) {
    try {
      const delet = await this.testService.remove(testid);
      return response.status(HttpStatus.OK).json({
        message: 'test supprimé avec succès',
        data: delet,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
