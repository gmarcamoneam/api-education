import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateTestDto } from './dto/create-test.dto';
import { UpdateTestDto } from './dto/update-test.dto';

import { User, UserDocument } from 'src/users/shemas/user.shemas';

import { Test, TestDocument } from './shemas/tests.shemas';

@Injectable()
export class TestService {
  constructor(
    @InjectModel(Test.name)
    private testModel: Model<TestDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  async create(createTestDto: CreateTestDto): Promise<Test> {
    const newtest = await new this.testModel(createTestDto);
    await this.userModel
      .findOneAndUpdate(
        { _id: createTestDto.formateur },
        { $push: { tests: newtest._id } }, // Pousse l'ID du cours dans le tableau cours de l'utilisateur
        { new: true }, // Pour renvoyer l'utilisateur mis à jour
      )
      .exec();
    // return newtest.save();
    return (await newtest.save()).populate({
      path: 'formateur',
      model: 'User',
    });
  }
  async findAll() /*: Promise<Test[]> */ {
    const listtest = await this.testModel
      .find()
      .populate({
        path: 'formateur',
        model: 'User',
      })
      .exec();
    if (!listtest || listtest.length == 0) {
      throw new NotFoundException('Données des tests non trouvées !');
    }
    return { total: listtest.length, listtest };
  }
  async findOne(id: number): Promise<Test> {
    const existingtest = await this.testModel
      .findById(id)
      .populate({
        path: 'formateur',
        model: 'User',
      })
      .exec();
    if (!existingtest) {
      throw new NotFoundException(`Test #${id} not found`);
    }
    return existingtest;
  }
  async update(id: number, updateTestDto: UpdateTestDto): Promise<Test> {
    const existingtest = await this.testModel.findByIdAndUpdate(
      id,
      updateTestDto,
      { new: true },
    );
    if (!existingtest) {
      throw new NotFoundException(`test #${id} not found`);
    }
    return existingtest;
  }
  async remove(testid: number): Promise<Test> {
    const deletetests = await this.testModel.findByIdAndDelete(testid);

    if (!deletetests) {
      throw new Error(`Tests with ID ${testid} not found`);
    }

    // Remove the test ID from the tests array of the corresponding user
    const updatedUser = await this.userModel.findByIdAndUpdate(
      deletetests.formateur,
      { $pull: { tests: deletetests._id } },
      { new: true },
    );

    if (!updatedUser) {
      throw new Error(`User with ID ${deletetests.formateur} not found`);
    }

    return deletetests;
  }
}
