// test.entity.ts
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { UserSchema } from 'src/users/shemas/user.shemas';

export type TestDocument = Test & Document;

@Schema({timestamps:true})
export class Test {
  @Prop()
  durer: string;

  @Prop()
  description: string;

  @Prop(UserSchema)
  formateur: string;

  @Prop(UserSchema)
  etudiant: string;

//   @Prop({ default: Date.now })
//   createdAt: Date;
}

export const TestSchema = SchemaFactory.createForClass(Test);
