import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { AccessTokenStrategy } from './strategies/accessToken.strategy';
import { RefreshTokenStrategy } from './strategies/refreshToken.strategy';
import { UsersModule } from 'src/users/users.module';
import { FormateurStrategy } from './strategies/formateur.strategy';
import { AdminStrategy } from './strategies/admin.strategy';
import { EtudiantStrategy } from './strategies/etudiant.strategy';
import { GoogleStrategy } from './strategies/google.strategy';

@Module({
  imports: [UsersModule, JwtModule.register({})],
  controllers: [AuthController],
  providers: [
    AuthService,
    AccessTokenStrategy,
    RefreshTokenStrategy,
    FormateurStrategy,
    AdminStrategy,
    EtudiantStrategy,
    GoogleStrategy,
  ],
})
export class AuthModule {}
