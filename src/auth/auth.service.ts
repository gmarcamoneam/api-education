import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt/dist';
import { UsersService } from 'src/users/users.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { ConfigService } from '@nestjs/config';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import {
  BadRequestException,
  ForbiddenException,
} from '@nestjs/common/exceptions';
import * as argon2 from 'argon2';
import { MailerService } from '@nestjs-modules/mailer/dist';
import { randomBytes } from 'crypto';
import * as bcrypt from 'bcrypt';
@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
    private mailerService: MailerService,
  ) {}
  //signup admin
  async signUp(createUserDto: CreateUserDto): Promise<any> {
    const userExists = await this.usersService.findByEmail(createUserDto.email);
    if (userExists) {
      throw new BadRequestException('User already exists');
    }
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createUserDto.password, salt); 
    // await this.sendMailRegisterUser(createUserDto);
    const newUser = await this.usersService.create({
      ...createUserDto,
      password: hash,    
      role: 'admin',  
      confirmed: true,   
    });    
    console.log(newUser);

    const tokens = await this.getTokens(
      newUser._id,
      newUser.email,
      newUser.role,
    );
    await this.updateRefreshToken(newUser._id, tokens.refreshToken);
    return { newUser, tokens };
  }
  //singnUp etudiant
  async signUpEtudiant(createUserDto: CreateUserDto): Promise<any> {
    const userExists = await this.usersService.findByEmail(createUserDto.email);
    if (userExists) {
      throw new BadRequestException('User already exists');
    }
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createUserDto.password, salt);
    createUserDto.vreified = randomBytes(6).toString('hex');
    await this.sendMailRegisterUser(createUserDto);
    const newUser = await this.usersService.create({
      ...createUserDto,
      password: hash,
      role: 'etudiant',
      confirmed: false,
    });
    console.log(newUser);

    const tokens = await this.getTokens(
      newUser._id,
      newUser.email,
      newUser.role,
    );
    await this.updateRefreshToken(newUser._id, tokens.refreshToken);
    return { newUser, tokens };
  }
  //singnUp fomateur
  async signUpFormateur(createUserDto: CreateUserDto): Promise<any> {
    // Check if user exists
    const userExists = await this.usersService.findByEmail(createUserDto.email);
    if (userExists) {
      throw new BadRequestException('User already exists');
    }
    const salt = await bcrypt.genSalt();
    const hash = await bcrypt.hash(createUserDto.password, salt);
    // await this.sendMailRegisterUser(createUserDto);
    const newUser = await this.usersService.create({
      ...createUserDto,
      password: hash,
      role: 'formateur',
      // confirmed: false,
      accepted: false,
    });
    console.log(newUser);

    const tokens = await this.getTokens(
      newUser._id,
      newUser.email,
      newUser.role,
    );
    await this.updateRefreshToken(newUser._id, tokens.refreshToken);
    return { newUser, tokens };
  }

  //login
  async signIn(data: CreateAuthDto) {
    const { email, password } = data;
    // Check if user exists
    const user = await this.usersService.findByEmail(data.email);
    console.log('user', user);
    if (!user) throw new BadRequestException('User does not exist');
    const passwordMatches = bcrypt.compareSync(password, user.password);
    console.log('passwordMatches', passwordMatches);
    if (!passwordMatches) 
      throw new BadRequestException('Password is incorrect');
    const tokens = await this.getTokens(user._id, user.email, user.role);
    console.log('tokens', tokens);
    await this.updateRefreshToken(user._id, tokens.refreshToken);
    return { user, tokens };
    // return {  tokens };

  }

  //logout
  async logout(userId: string) { 
    return this.usersService.update(userId, { refreshToken: null });
  }

  //hashing password et data
  hashData(data: string) {
    return argon2.hash(data);
  }

  //update refrech token
  async updateRefreshToken(userId: string, refreshToken: string) {
    const hashedRefreshToken = await this.hashData(refreshToken);
    await this.usersService.update(userId, {
      refreshToken: hashedRefreshToken,
    });
  }

  //token
  async getTokens(userId: string, email: string, role: string) {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          sub: userId,
          email,
          role,
        },
        {
          secret: this.configService.get<string>('JWT_ACCESS_SECRET'),
          expiresIn: '1d',
        },
      ),
      this.jwtService.signAsync(
        {
          sub: userId,
          email,
          role,
        },
        {
          secret: this.configService.get<string>('JWT_REFRESH_SECRET'),
          expiresIn: '7d',
        },
      ),
    ]);

    return {
      accessToken,
      refreshToken,
    };
  }
  //refteshtoken
  async refreshTokens(userId: string, refreshToken: string) {
    const user = await this.usersService.findById(userId);
    if (!user || !user.refreshToken)
      throw new ForbiddenException('Access Denied');
    const refreshTokenMatches = await argon2.verify(
      user.refreshToken,
      refreshToken,
    );
    if (!refreshTokenMatches) throw new ForbiddenException('Access Denied');
    const tokens = await this.getTokens(user._id, user.email, user.role);
    await this.updateRefreshToken(user.id, tokens.refreshToken);
    return tokens;
  }
  //site send email
  private sendMailRegisterUser(user): void { 
    this.mailerService
      .sendMail({
        to: user.email,
        subject: 'welcome!' + ' ' + user.firstname + ' ' + user.lastname,
        text: 'Bonjour Mr/Mme',
        html: `
                            <h2>Hello ${user.firstname} ${user.lastname}</h2>
                            <h3>we are glad to have you on board at  ${user.firstname}. </h3>
                            <h3>Please click the following link to confirm your email:</h3>
                            <h3> <a href="http://localhost:4000/auth/confirm-now/${user.vreified}">Confirm Email</a> </h3>
                     `,
      })
      .then((response) => {
        console.log(response);
        console.log('User Registration: Send Mail successfully!');
      })
      .catch((err) => {
        console.log(err);
        console.log('User Registration: Send Mail Failed!');
      });
  }

  // googleLogin(req) {
  //   if (!req.user) {
  //     return 'No user from google'
  //   }

  //   return {
  //     message: 'User information from google',
  //     user: req.user
  //   }
  // }

 
}
