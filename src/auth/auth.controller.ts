import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  UseGuards,
  HttpStatus,
} from '@nestjs/common';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { CreateAuthDto } from './dto/create-auth.dto';
import { UpdateAuthDto } from './dto/update-auth.dto';
import { Request, Response } from 'express';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
import { RefreshTokenGuard } from 'src/common/guards/refreshToken.guard';
import { Res, UploadedFile, UseInterceptors } from '@nestjs/common/decorators';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { BadRequestException } from '@nestjs/common/exceptions';
import { UsersService } from 'src/users/users.service';
import { GoogleAuthGuard } from './strategies/guards';
import { AuthGuard } from '@nestjs/passport';
import * as path from 'path';
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}
  //signup admin
  @Post('signup')
  signup(@Body() createUserDto: CreateUserDto) {
    return this.authService.signUp(createUserDto);
  }
  //sinup etudiant
  @Post('signupetudiant')
  @UseInterceptors( 
    FileInterceptor('picture', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(
            null,
            file.fieldname + '-' + uniqueSuffix + extname(file.originalname),
          );
        },
      }),
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|webp)$/)) {
          return cb(
            new BadRequestException('Only image files are allowed!'),
            false,
          );
        }
        cb(null, true);
      },
    }),
  )
  signupetudiant(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() picture: Express.Multer.File,            
  ) {
    console.log(picture);
    if (!picture) {
      throw new BadRequestException('Une photo est obligatoire');
    }
    createUserDto.picture = picture.filename; 
    return this.authService.signUpEtudiant(createUserDto);
  }
  //sinup formateur
  @Post('signupformateur')
  @UseInterceptors(
    FileInterceptor('picture', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(
            null,
            file.fieldname + '-' + uniqueSuffix + extname(file.originalname),
          );
        },
      }),
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|webp|pdf)$/)) {
          return cb(
            new BadRequestException('Only image files are allowed!'),
            false,
          );
        }
        cb(null, true);
      },
    }),
  )
  signupformateur(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() picture: Express.Multer.File,
  ) {
    console.log(picture);
    // if (picture) {
    //   createUserDto.picture = picture.filename;
    // }
    if (!picture) {
      throw new BadRequestException('Une photo est obligatoire');
    }
    createUserDto.picture = picture.filename;
    return this.authService.signUpFormateur(createUserDto);
  }
  // les trois fonctions suivantes pour afficher une image dans le postman et consommer cette image dans l'angular
  @Post('upload-photo')
  @UseInterceptors(
    FileInterceptor('picture', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, cb) => {
          const name = file.originalname.split('.')[0];
          const fileExtension = file.originalname.split('.')[1];
          const newFileName =
            name.split(' ').join('_') + '_' + Date.now() + '.' + fileExtension;
          cb(null, newFileName);
        },
      }),
      fileFilter: (req, file, cb) => {
        if (!file.originalname.match(/\.(jpg|jpeg|png|webp|pdf)$/)) {
          return cb(
            new BadRequestException('Only image files are allowed!'),
            false,
          );
        }
        cb(null, true);
      },
    }),
  )
  uploadPhoto(@UploadedFile() picture: Express.Multer.File) {
    if (!picture) {
      throw new BadRequestException('file is not an image');
    } else {
      const response = {
        filePath: `http://localhost:4000/auth/pictures/${picture.filename}`,
      };
      return response;
    }
  }
  @Get('pictures/:filename')
  async getPictures(@Param('filename') filename, @Res() res: Response) {
    res.sendFile(filename, { root: './uploads' });
  }

  //signin ||login
  @Post('signin')
  signin(@Body() data: CreateAuthDto) {
    return this.authService.signIn(data);
  }

  //signin ||login GoogleAuthGuard
  @Get('signin')
  @UseGuards(GoogleAuthGuard)
  async signinn(@Body() data: CreateAuthDto) {
    //  return this.authService.signIn(data);
    const user = await this.authService.signIn(data);
    return { message: 'Google Authenticated', user };
  }

  // @Get('signin')
  // @UseGuards(GoogleAuthGuard)
  // handleLogin() {
  //   return { msg: 'Google Authentication' };
  // }
  //logout||deconnecteer
  @UseGuards(AccessTokenGuard)
  @Get('logout')
  async logout(@Req() req: Request,@Res() response,) {
    // this.authService.logout(req.user['sub']);
    try {
    const logoutt = await this.authService.logout(req.user['sub'])
    response.status(HttpStatus.OK).json({
      message:"deconnected successfully",
      status:HttpStatus.CREATED,
      data:logoutt
     })
    } catch (error) {
      response.status(HttpStatus.BAD_REQUEST).json({
        message:"failed to deconnected  ",
        status:HttpStatus.BAD_REQUEST,
        data:null})
  }
}
  //refreshtoken
  @UseGuards(RefreshTokenGuard)
  @Get('refresh')
  refreshTokens(@Req() req: Request) {
    const userId = req.user['sub'];
    const refreshToken = req.user['refreshToken'];
    return this.authService.refreshTokens(userId, refreshToken);
  }
  //forgetpassword
  @Post('/forget')
  forget(@Body() createuserdto: CreateUserDto) {
    return this.usersService.forgetPassword(createuserdto);
  }
  // confirmation email
  @Get('confirm-now/:vreified')
  async verifyEmail(
    @Param('vreified') vreified: string,
    @Res() res: any,
  ): Promise<any> {
    try {
      const user = await this.usersService.verifyEmail(vreified);
      res.send(`
<html>
  <head>
    <style>
      /* Ajouter ici vos styles CSS personnalisés */
      body {
        font-family: Arial, sans-serif;
        background-color: #f8f8f8;
      }
      .container {
        width: 400px;
        margin: 0 auto;
        padding: 20px;
        background-color: #ffffff;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
        border-radius: 5px;
      }
      h2 {
        color: #33cc33;
        margin-top: 0;
      }
      p {
        color: #666666;
        margin-top: 10px;
      }
      .login-button {
        background-color: #33cc33;
        color: #ffffff;
        padding: 10px 20px;
        border: none;
        cursor: pointer;
        margin-top: 20px;
        border-radius: 3px;
      }
      .error {
        color: #ff0000;
        margin-top: 10px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h2>Email Confirmed Successfully</h2>
      <p>Thank you for confirming your email address.</p>
      <p>You can now log in to your account.</p>
     
    </div>
  
  </body>
</html>
`);
    } catch (error) {
      res.send(`
<html>
  <head>
    <style>
      /* Ajouter ici vos styles CSS personnalisés */
      body {
        font-family: Arial, sans-serif;
        background-color: #f8f8f8;
      }
      .container {
        width: 400px;
        margin: 0 auto;
        padding: 20px;
        background-color: #ffffff;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
        border-radius: 5px;
      }
      h2 {
        color: #ff0000;
        margin-top: 0;
      }
      p {
        color: #666666;
        margin-top: 10px;
      }
      .error {
        color: #ff0000;
        margin-top: 10px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <h2>Email Confirmation Failed</h2>
      <p>Failed to confirm email address. Please try again later.</p>
      <p class="error">Error: ${error.message}</p>
    </div>
  </body>
</html>
`);
    }
  }
  //resetpassword
  @Post('reset/:token')
  async reset(@Body() body: any, @Param('token') token: string, @Res() res) {
    try {
      await this.usersService.resetpassword(body.newpassword, token);
      return res.status(HttpStatus.OK).json({
        message: 'Request Change Password Successfully!',
        status: 200,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: Change password failed!',
        status: 400,
      });
    }
  }

  @UseGuards(AuthGuard('admin'))
  @Get('confirm/:id')
  async confirmerCompteFormateur(@Param('id') id: string) {
    // return this.usersService.confirmerCompteFormateur(id);
    const accept = await this.usersService.confirmerCompteFormateur(id);
    if (accept) {
      return { message: 'Compte confirmer avec succès', accept };
    } else {
      return { message: 'ID de formateur invalide' };
    }
  }

  @Get('allformateur')
  async getFormateurs(@Res() response) {
    // return this.usersService.getFormateurs();
    try {
      const listformateurs = await this.usersService.getFormateurs();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des formateurs ont été trouvées avec succès',
        data: listformateurs,
      });
    } catch (err) {
      response.status(400).send({ message: err.message });
    }
  }

  @Get('alletudiant')
  async getEtudiants() {
    return this.usersService.getEtudiants();
  }

  // @Post()
  // create(@Body() createAuthDto: CreateAuthDto) {
  //   return this.authService.create(createAuthDto);
  // }

  // @Get()
  // findAll() {
  //   return this.authService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.authService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateAuthDto: UpdateAuthDto) {
  //   return this.authService.update(+id, updateAuthDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.authService.remove(+id);
  // }
}
