import {
  Injectable,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AdminStrategy extends PassportStrategy(Strategy, 'admin') {
  constructor(private readonly userservice: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.APP_SECRET,
    });
  }

  async validate(payload: any) {
    const user = await this.userservice.validateUser(payload);
    //   console.log(user);
    if (!user) {
      throw new UnauthorizedException();
    }
    if (user.role !== 'admin') {
      throw new ForbiddenException('Permission  Denied');
    }
    return user;
  }
}      
