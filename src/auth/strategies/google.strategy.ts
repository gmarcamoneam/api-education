import { Inject, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Profile, Strategy } from 'passport-google-oauth20';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy) {
  constructor(
    // @Inject('AUTH_SERVICE') private readonly authService: AuthService,
    // @Inject('USERS_SERVICE') private readonly usersService: UsersService,
    private readonly userservice: UsersService,
  ) {
    super({
      //   clientID: '', 
      //   clientSecret: '',
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.CODE_SECRET_CLIENT,
      callbackURL: 'http://localhost:4000/auth/signin',
      scope: ['profile', 'email'], 
    }); 
  }

  async validate(accessToken: string, refreshToken: string, profile: Profile) {
    console.log('accessToken', accessToken);
    console.log('refreshToken', refreshToken);
    console.log('profile', profile);
    const user = await this.userservice.validateUser({
      email: profile.emails[0].value,
      displayName: profile.displayName,
      // displayName: profile.name,
    });
    console.log('Validate');
    console.log('user', user);
    return user || null;
  }

  // async validate(accessToken: string, refreshToken: string, profile: any) {
  //  const user = await  this.userservice.validateGoogleUser(profile);
  //  return {
  //   accessToken,
  //   refreshToken,
  //   user
  //  }
  // }
}    
    