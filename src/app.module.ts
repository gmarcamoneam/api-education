import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { join } from 'path';
import { CoursModule } from './cours/cours.module';
import { MulterModule } from '@nestjs/platform-express/multer';
import { FormationModule } from './formation/formation.module';
import { PassportModule } from '@nestjs/passport';
import { PaiementModule } from './paiement/paiement.module';

import { TestModule } from './test/test.module';
// import { SocketsGateway } from './sokets/sokets.gateway';
// import { MessageModule } from './message/message.module';

import { CorsMiddleware } from '@nest-middlewares/cors';

import * as cors from 'cors';

@Module({
  imports: [
    UsersModule,
    ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env',}),
    MongooseModule.forRoot(process.env.APP_DB,{
      // useNewUrlParser: true,
      // useUnifiedTopology: true,
    }),
    MulterModule.register({
      dest: '/uploads',
    }),
    AuthModule,
    PassportModule, //passport et stripe
    MailerModule.forRootAsync({
      useFactory: () => ({
        transport: {
          service: process.env.GMAIL_SERVICE,
          host: process.env.EMAIL_HOST,
          port: 2525,
          // ignoreTLS: true,
          secure: true,
          auth: {
            user: process.env.EMAIL_AUTH_USER, 
            pass: process.env.EMAIL_AUTH_PASSWORD,
          },
        },
        defaults: {
          from: '"nest-modules" <modules@nestjs.com>',
        },
        //  preview: true,
        // template: {
        //   dir: process.cwd() + '/templates/emails',
        //   adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
        //   options: {
        //     strict: true,
        //   },
        // },
      }),
    }),
    // MailerModule.forRootAsync({
    //   useFactory: () => ({
    //     transport: {
    //       host: 'smtp.mailtrap.io',
    //       // service:"gmail",
    //       // host: "smtp.gmail.com",

    //       port: 2525,
    //       secure: false, //kanet true badaltha false
    //       auth: {
    //         user: '03aade3ded14ab', //user de mailtrap
    //         // user:  "gmarcamoneam@gmail.com",
    //         pass: 'fb1c1dbaecd53f', //mot de passe de mailtrap
    //         // pass:  "rwrcwtddvclchzpf"
    //       },
    //       tls: {
    //         rejectUnauthorized: false,
    //         ciphers: 'SSLv3', //les 3 ligne zedethhom mba3ed
    //       },
    //     },
    //     defaults: {
    //       from: '"nest-modules" <modules@nestjs.com>',
    //     },
    //     // template: {
    //     //   dir: process.cwd() + './templates/emails',
    //     //   adapter: new HandlebarsAdapter(), // or new PugAdapter() or new EjsAdapter()
    //     //   options: {
    //     //     strict: true,
    //     //   },
    //     // },
    //   }),
    // }),
    CoursModule,
    FormationModule,
    PaiementModule,
    TestModule,
    // MessageModule,
  ],

  controllers: [AppController],
  providers: [AppService /*SocketsGateway*/],
})
export class AppModule implements NestModule {
  //    constructor(){
  //   console.log(join(__dirname,'template/errors.html'))
  // }
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(
        cors({
          origin: 'http://localhost:4200', // autorise les demandes en provenance du domaine local
          methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
          credentials: true, // autorise les cookies
        }),
      )
      .forRoutes('*');
  }
}
