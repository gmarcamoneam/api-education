import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { UserSchema } from 'src/users/shemas/user.shemas';

export type CourDocument = Cour & Document;

@Schema({ timestamps: true })
export class Cour {
  @Prop()
  titre: string;

  @Prop()
  description: string;
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
id_formateur: string;

  @Prop(UserSchema)
  id_etudiant: string;
}

export const CourSchema = SchemaFactory.createForClass(Cour);
