import { Module } from '@nestjs/common';
import { CoursService } from './cours.service';
import { CoursController } from './cours.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Cour, CourSchema } from './shemas/cours.shemas';
import { User, UserSchema } from 'src/users/shemas/user.shemas';
import { UsersService } from 'src/users/users.service';
import { UsersModule } from 'src/users/users.module';
import { TestModule } from 'src/test/test.module';
import { Test, TestSchema } from 'src/test/shemas/tests.shemas';

@Module({
  imports: [CoursModule,UsersModule,TestModule,
    MongooseModule.forFeature([{ name: Cour.name, schema: CourSchema },
      { name: User.name, schema: UserSchema },{ name: Test.name, schema: TestSchema }]),
  ],
  controllers: [CoursController],
  providers: [CoursService,UsersService],
}) 
export class CoursModule {}
