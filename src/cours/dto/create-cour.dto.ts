import { ObjectId } from 'mongoose';

export class CreateCourDto {
  titre: string;
  description: string;
  // id_formateur:string
  // id_formateur: { type: ObjectId; ref: 'User' };
  readonly id_formateur: {
    _id: ObjectId;
    firstname: string;
    lastname: string;
    email: string;
    address: string;
    phone: string;
    picture: string;
    domaine:string
  };
  id_etudiant: { type: ObjectId; ref: 'User' };
}
