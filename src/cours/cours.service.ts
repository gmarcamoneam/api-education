import { Injectable } from '@nestjs/common';
import { NotFoundException } from '@nestjs/common/exceptions';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/users/shemas/user.shemas';
import { CreateCourDto } from './dto/create-cour.dto';
import { UpdateCourDto } from './dto/update-cour.dto';
import { Cour, CourDocument } from './shemas/cours.shemas';

@Injectable()
export class CoursService {
  constructor(
    @InjectModel(Cour.name) private courModel: Model<CourDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  async create(createCourDto: CreateCourDto): Promise<Cour> {
    const newCour = await this.courModel.create(createCourDto); // Crée le cours
    await this.userModel
      .findOneAndUpdate(
        { _id: createCourDto.id_formateur },
        { $push: { cours: newCour._id } }, // Pousse l'ID du cours dans le tableau cours de l'utilisateur
        { new: true }, // Pour renvoyer l'utilisateur mis à jour
      )
      .exec();
    // return newCour;
    // return newCour.save();
    return (await newCour.save()).populate({
      path: 'id_formateur',
      model: 'User',
    });
  }

  async findAll() /*: Promise<Cour[]>*/ {
    const listcour = await this.courModel
      .find()
      .populate({ path: 'id_formateur', model: 'User' })
      .exec();
    if (!listcour || listcour.length == 0) {
      throw new NotFoundException('Données des cours non trouvées !');
    }
    return { total: listcour.length, listcour };
  }
  // (/*........*/ )
  // async findAll(page: number = 1, limit: number/* = 10*/): Promise<{ total: number, cour: Cour[] }> {
  //   const skip = (page - 1) * limit;
  //   const [count, cour] = await Promise.all([
  //     this.courModel.countDocuments(),
  //     this.courModel
  //       .find()
  //      // .populate({ path: "id_formateur", model: "User" })
  //       .skip(skip)
  //       .limit(limit)
  //       .exec(),
  //   ]);
  //   if (cour.length == 0) {
  //     throw new NotFoundException('Données des cours non trouvées !');
  //   }
  //   return { total: count, cour };
  // }
  // async findAll(
  //   page: number = 1,
  //   limit: number,
  //   search: string = '',
  // ): Promise<{ total: number; cour: Cour[] }> {
  //   const skip = (page - 1) * limit;
  //   const query = search ? { titre: { $regex: search, $options: 'i' } } : {};

  //   const [count, cour] = await Promise.all([
  //     this.courModel.countDocuments(query),
  //     this.courModel
  //       .find(query)
  //       // .populate({ path: "id_formateur", model: "User" })
  //       .skip(skip)
  //       .limit(limit)
  //       .exec(),
  //   ]);
  //   if (cour.length == 0) {
  //     throw new NotFoundException('Données des cours non trouvées !');
  //   }
  //   return { total: count, cour };
  // }

  async findOne(courId: string): Promise<Cour> {
    const existingcour = await this.courModel
      .findById(courId)
      // .populate({ path: 'id_formateur', model: 'User' })
      .populate({
        path: 'id_formateur',
        model: 'User',
        populate: {
          path: 'cours',
          model: 'Cour',
        },
      })
      .exec();
    if (!existingcour) {
      throw new NotFoundException(`cour #${courId} not found`);
    }
    return existingcour;
  }

  async update(courId: string, updatecourDto: UpdateCourDto): Promise<Cour> {
    const existingcour = await this.courModel.findByIdAndUpdate(
      courId,
      updatecourDto,
      { new: true },
    );
    if (!existingcour) {
      throw new NotFoundException(`cour #${courId} not found`);
    }
    return existingcour;
  }

  async remove(courId: string): Promise<Cour> {
    const deletecours = await this.courModel.findByIdAndDelete(courId);

    if (!deletecours) {
      throw new Error(`Cours with ID ${courId} not found`);
    }

    // Remove the test ID from the tests array of the corresponding user
    const updatedUser = await this.userModel.findByIdAndUpdate(
      deletecours.id_formateur,
      { $pull: { cours: deletecours._id } },
      { new: true },
    );

    if (!updatedUser) {
      throw new Error(`User with ID ${deletecours.id_formateur} not found`);
    }

    return deletecours;
  }
}
