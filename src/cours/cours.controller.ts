import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  Query,
} from '@nestjs/common';
import { UseGuards } from '@nestjs/common/decorators';

import { HttpStatus } from '@nestjs/common/enums';
import { AuthGuard } from '@nestjs/passport';
import { CoursService } from './cours.service';
import { CreateCourDto } from './dto/create-cour.dto';
import { UpdateCourDto } from './dto/update-cour.dto';
import { Cour } from './shemas/cours.shemas';

@Controller('cours')
export class CoursController {
  constructor(private readonly coursService: CoursService) {}

  @UseGuards(AuthGuard('formateur'))
  @Post('/create')
  async create(@Res() response, @Body() createcourDto: CreateCourDto) {
    try {
      const newcors = await this.coursService.create(createcourDto);
      return response.status(HttpStatus.CREATED).json({
        message: 'cour a été créé avec succès',
        data: newcors,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Erreur : cour non créé !',
        erreur: 'Bad Request',
      });
    }
  }
  // @Get('/getall')
  // async findAll(
  //   @Query('page') page: number,
  //   @Query('limit') limit: number,
  //   @Query('search') search: string,
  // ): Promise<{ total: number; cour: Cour[] }> {
  //   return this.coursService.findAll(page, limit, search);
  // }
  @Get('/getall')
  async findAll(@Res() response) {
    try {
      const listcours = await this.coursService.findAll();
      return response.status(HttpStatus.OK).json({
        message: 'Toutes les données des cours ont été trouvées avec succès',
        data: listcours,
      });
    } catch (err) {
      response.status(400).send({ message: err.message });
    }
  }
  @Get('/getbyid/:id')
  async findOne(@Res() response, @Param('id') courId: string) {
    try {
      const existingcour = await this.coursService.findOne(courId);
      return response.status(HttpStatus.OK).json({
        message: 'cour trouvé avec succès',
        data: existingcour,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @UseGuards(AuthGuard('formateur'))
  @Patch('/update/:id')
  async update(
    @Res() response,
    @Param('id') courid: string,
    @Body() updatecourDto: UpdateCourDto,
  ) {
    try {
      const existingcour = await this.coursService.update(
        courid,
        updatecourDto,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Le cour a été mis à jour avec succès',
        data: existingcour,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @UseGuards(AuthGuard('formateur'))
  @Delete('/delete/:id')
  async remove(@Res() response, @Param('id') courId: string) {
    try {
      const Delete = await this.coursService.remove(courId);
      return response.status(HttpStatus.OK).json({
        message: 'cour supprimé avec succès',
        // data: 0,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
