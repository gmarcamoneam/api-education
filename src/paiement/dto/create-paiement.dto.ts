import { ObjectId } from 'mongoose';

export class CreatePaiementDto {
  date: Date;
  montant: number;
  // etudiant: { type: ObjectId; ref: 'User' };
  readonly etudiant: {
    _id: ObjectId;
    firstname: string;
    lastname: string;
    email: string;
    address: string;
    phone: string;
    picture: string;
  };
  // formation: { type: ObjectId; ref: 'Formation' };
  readonly formation: {
    _id: ObjectId;
    type: string;
    dureer: string;
    prix: string;
    heure:string
  };
}
