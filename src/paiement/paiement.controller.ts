import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { PaiementService } from './paiement.service';
import { CreatePaiementDto } from './dto/create-paiement.dto';
import { UpdatePaiementDto } from './dto/update-paiement.dto';
import { AuthGuard } from '@nestjs/passport';
import { Paiement } from './shemas/paiement.shemas';
import { User } from 'src/users/shemas/user.shemas';

@Controller('paiement')
export class PaiementController {
  constructor(private readonly paiementService: PaiementService) {}

  @UseGuards(AuthGuard('etudiant'))
  @Post('create')
  create(@Body() createPaiementDto: CreatePaiementDto) {
    return this.paiementService.createPaiement(createPaiementDto);
  }

  @Get('getall')
  findAll() {
    return this.paiementService.getAllPaiement();
  }

  @Get('getbyid/:id')
  findOne(@Param('id') id: string) {
    return this.paiementService.getPaiementById(id);
  }

  @UseGuards(AuthGuard('etudiant'))
  @Patch('update/:id')
  update(
    @Param('id') id: string,
    @Body() updatePaiementDto: UpdatePaiementDto,
  ) {
    return this.paiementService.updatePaiement(id, updatePaiementDto);
  }

  // @UseGuards(AuthGuard('etudiant'))           
  @Delete('delete/:id')
  remove(@Param('id') id: string) {
    return this.paiementService.deletePaiement(id);
  }
}     
   