import { Module } from '@nestjs/common';
import { PaiementService } from './paiement.service';
import { PaiementController } from './paiement.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Paiement, PaiementSchema } from './shemas/paiement.shemas';
import { MailerModule } from '@nestjs-modules/mailer';
import { UsersModule } from 'src/users/users.module';
import { FormationModule } from 'src/formation/formation.module';
import { User, UserSchema } from 'src/users/shemas/user.shemas';
import { Fiormation, FormationSchema } from 'src/formation/shemas/formation.shemas';
import { UsersService } from 'src/users/users.service';
import { FormationService } from 'src/formation/formation.service';
import { Cour, CourSchema } from 'src/cours/shemas/cours.shemas';
import { Test, TestSchema } from 'src/test/shemas/tests.shemas';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports: [ 
   // UsersModule,FormationModule,PaiementModule,  //lazem l importation l kol 7aja 3andha relation bl les classe lkol
  MongooseModule.forFeature([{ name: Paiement.name, schema: PaiementSchema }]),
  MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  MongooseModule.forFeature([{ name: Fiormation.name, schema: FormationSchema }]),
  MongooseModule.forFeature([{ name: Cour.name, schema: CourSchema }]),
  MongooseModule.forFeature([{ name: Test.name, schema: TestSchema }]),





],
  controllers: [PaiementController],
  providers: [PaiementService,UsersService,FormationService,JwtService]
})
export class PaiementModule {}
