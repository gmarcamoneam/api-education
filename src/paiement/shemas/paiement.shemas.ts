import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import {
  Fiormation,
  FormationSchema,
} from 'src/formation/shemas/formation.shemas';
import { UserSchema } from 'src/users/shemas/user.shemas';
// import { Etudiant } from './etudiant.schema';
// import { Formation } from './formation.schema';

export type PaiementDocument = Paiement & Document;

@Schema({timestamps: true})
export class Paiement {
  @Prop({ type: Date, default: Date.now })
  date: Date;
 

  @Prop({ required: true, type: Number })
  montant: number;

  @Prop(UserSchema)
  etudiant: string;



  @Prop(FormationSchema)
  formation: string;
}

export const PaiementSchema = SchemaFactory.createForClass(Paiement);

// date: La date à laquelle le paiement a été effectué.
// montant: Le montant du paiement.
// modePaiement: Le mode de paiement utilisé (par exemple, carte de crédit, PayPal, etc.).
// estValide: Un indicateur booléen qui indique si le paiement est valide.
// estRembourse: Un indicateur booléen qui indique si le paiement a été remboursé.
// transactionId: L'identifiant de transaction du paiement (par exemple, l'ID de transaction de Stripe ou PayPal).
// etudiant: Une référence à l'étudiant qui a effectué le paiement.
// formation: Une référence à la formation pour laquelle le paiement a été effectué.
