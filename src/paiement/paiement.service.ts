import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from 'src/users/shemas/user.shemas';
import { CreatePaiementDto } from './dto/create-paiement.dto';
import { UpdatePaiementDto } from './dto/update-paiement.dto';
import { Paiement, PaiementDocument } from './shemas/paiement.shemas';
import * as nodemailer from 'nodemailer';
import {
  Fiormation,
  FormationDocument,
} from 'src/formation/shemas/formation.shemas';

@Injectable()
export class PaiementService {
  constructor(
    @InjectModel(Paiement.name) private paiementModel: Model<PaiementDocument>,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Fiormation.name)
    private formationModel: Model<FormationDocument>,
  ) {}
  async createPaiement(
    createpaiementdto: CreatePaiementDto,
  ): Promise<Paiement> {
    const createdPaiement = new this.paiementModel(createpaiementdto);
    console.log('createdpaiement', createdPaiement);
    await createdPaiement.save();
    //  return ((await (await createdPaiement.save()).populate({ path: "etudiant", model: "User" })).populate({ path: "formation", model: "Fiormation" }));

    // Envoyer une notification par e-mail à l'administrateur
    await this.sendNotification(createpaiementdto);
    return createdPaiement.toObject();
  }

  async sendNotification(createpaiementdto) {
    const transporter = nodemailer.createTransport({
      service: process.env.GMAIL_SERVICE,
      host: process.env.EMAIL_HOST,
      port: 2525,
      auth: {
        // user: 'admin@gmail.com', // Adresse e-mail de l'administrateur
        // pass: 'admin_password', // Mot de passe de l'administrateur
        user: process.env.EMAIL_AUTH_USER,
        pass: process.env.EMAIL_AUTH_PASSWORD,
      },
    });
    const userconnect = await this.userModel.findOne(
      createpaiementdto.etudiant._id,
    );
    console.log('userconnect,', userconnect);
    const formationexist = await this.formationModel.findOne(
      createpaiementdto.formation._id,
    );
    console.log('formationexist,', formationexist);

    const message = {
      from: 'admin@gmail.com',
      // to: 'admin@gmail.com', // Adresse e-mail de l'administrateur
      to: userconnect.email,
      subject: 'Nouvelle création de paiement',
      text: `L'étudiant ${userconnect.firstname} ${userconnect.lastname} (${userconnect.email}) a créé un nouveau paiement d'un montant de ${createpaiementdto.montant} pour la formation ${formationexist.type}`,
    };

    await transporter.sendMail(message);
  }

  async getAllPaiement() /*: Promise<Paiement[]>*/ {
    const listpaiement = await this.paiementModel
      .find()
      .populate({ path: 'etudiant', model: 'User' })
      .populate({ path: 'formation', model: 'Fiormation' })
      .exec();
    if (!listpaiement || listpaiement.length == 0) {
      throw new NotFoundException('Données des paiement non trouvées !');
    }
    return {
      message: 'toutes les paiement trouvées avec  ',
      total: listpaiement.length,
      listpaiement,
    };
  }
  async getPaiementById(paiementId: string): Promise<Paiement> {
    const existingpaiement = await this.paiementModel
      .findById(paiementId)
      .populate({ path: 'etudiant', model: 'User' })
      .populate({ path: 'formation', model: 'Fiormation' })
      .select('-updatedAt')
      .select('-createdAt')
      .select('-__v')
      .exec();
    if (!existingpaiement) {
      throw new NotFoundException(`Paiement #${paiementId} not exist`);
    }
    return existingpaiement;
  }
  async updatePaiement(
    paiementId: string,
    updatepaiementDto: UpdatePaiementDto,
  ): Promise<Paiement> {
    const existingpaiement = await this.paiementModel.findByIdAndUpdate(
      paiementId,
      updatepaiementDto,
      { new: true },
    );
    if (!existingpaiement) {
      throw new NotFoundException(`Paiement #${paiementId} not exist`);
    }
    return existingpaiement;
  }
  async deletePaiement(paiementid: string) /*: Promise<Paiement>*/ {
    const deletepaiement = await this.paiementModel.findByIdAndDelete(
      paiementid,
    );
    if (!deletepaiement) {
      throw new NotFoundException(`Paiement #${paiementid} not exist`);
    }
    return { message: 'deleted', deletepaiement };
  }
}
