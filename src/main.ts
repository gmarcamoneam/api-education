import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';
import { ConfigService } from '@nestjs/config/dist';
import { Logger } from '@nestjs/common';
import { WsAdapter } from '@nestjs/platform-ws';
import * as cors from 'cors';

// import { AuthenticatedSocketAdapter } from './sokets/authenticated.soket.adapter';
dotenv.config();
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useWebSocketAdapter(new WsAdapter(app)); 
  // app.useWebSocketAdapter(new AuthenticatedSocketAdapter(app)); // Add our custom socket adapter.
  const configService = app.get(ConfigService);
  app.enableCors({
    origin: [
      'http://localhost:4200', // angular
      //  'http://localhost:3000', // react
      // 'http://localhost:8081', // react-native     
    ],  
    //Google Auth20 
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true, // autorise les cookies
    // allowedHeaders: 'Content-Type, Authorization',   
  });
  app.use(cors());
  await app.listen(configService.get('APP_PORT'));
  Logger.log(
    `Server running on port: ${configService.get('APP_DOMAIN')}`,
    'Bootstrap',
  );
}
bootstrap();
