import { ObjectId } from 'mongoose';

export class CreateUserDto {
  email: string;
  firstname: string;
  lastname: string;
  password: string;
  date_de_naissance: Date;
  cin: string;
  gender: string;
  refreshToken: string;
  role: string;
  phone: string;
  vreified: string;
  confirmed: boolean; 
  resetPasswordToken: string;
  resetPasswordTokenAt: Date;
  picture: string;
  address: string;
  paiements: { type: ObjectId; ref: 'Paiement' };
  cours: { type: ObjectId; ref: 'Cour' };
  tests: { type: ObjectId; ref: 'Test' };
  formation: { type: ObjectId; ref: 'Fiormation' };//relation entre l'etudiant et le formation l'etudiant choisir une formation
  accepted: boolean; 
}
