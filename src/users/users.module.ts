import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User, UserSchema } from './shemas/user.shemas';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { MailerModule } from '@nestjs-modules/mailer';
import { Test, TestSchema } from 'src/test/shemas/tests.shemas';
import { Cour, CourSchema } from 'src/cours/shemas/cours.shemas';
import { TestService } from 'src/test/test.service';
import { CoursService } from 'src/cours/cours.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    MongooseModule.forFeature([{ name: Test.name, schema: TestSchema }]),
    MongooseModule.forFeature([{ name: Cour.name, schema: CourSchema }]),


  ],
  controllers: [UsersController],
  providers: [UsersService, JwtService,TestService,CoursService],
  exports: [UsersService],
})
export class UsersModule {}
