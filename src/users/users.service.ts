import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  Res,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Model } from 'mongoose';
import { User, UserDocument } from './shemas/user.shemas';
import { randomBytes } from 'crypto';
import { MailerService } from '@nestjs-modules/mailer';
import * as jwt from 'jsonwebtoken';
import { response } from 'express';
import * as bcrypt from 'bcrypt';

import { Cour, CourDocument } from 'src/cours/shemas/cours.shemas';
import { Test, TestDocument } from 'src/test/shemas/tests.shemas';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Cour.name) private courModel: Model<CourDocument>,
    @InjectModel(Test.name) private testModel: Model<TestDocument>,

    private mailerService: MailerService,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<UserDocument> {
    const createdUser = new this.userModel(createUserDto);
    return createdUser.save();
  }

  async findAll(): Promise<UserDocument[]> {
    return this.userModel.find().exec();
  }

  async findById(id: string): Promise<UserDocument> {
    return this.userModel.findById(id).exec();
  }

  async findByEmail(email: string): Promise<UserDocument> {
    return this.userModel.findOne({ email }).exec();
  }

  async update( 
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UserDocument> {
    return this.userModel
      .findByIdAndUpdate(id, updateUserDto, { new: true })
      .exec();
  }

  async remove(id: string): Promise<UserDocument> {
    return this.userModel.findByIdAndDelete(id).exec();
  }
  //formateur strategy
  async validateUser(payload: any): Promise<User> {
    return await this.userModel.findOne({ email: payload.email });
  }
  //google Auth
  async validateGoogleUser(profile: any): Promise<User> {
    const { email, name, picture } = profile._json;

    let user = await this.userModel.findOne({ email }).exec();

    if (!user) {
      user = new this.userModel({
        name,
        email,
        picture,
      });
      await user.save();
    }

    return user;
  }
  //forgetpassword
  async forgetPassword(createUserDto: CreateUserDto): Promise<any> {
    const userUpdate = await this.findByEmail(createUserDto.email);
    if (!userUpdate) {
      throw new NotFoundException('User email not exist !');
    }
    const resetPasswordToken = randomBytes(10).toString('hex'); // randomStringGenerator.toString()
    console.log('resetPasswordToken ', resetPasswordToken);
    const token = jwt.sign(
      {
        id: userUpdate._id,
        user: userUpdate,
      },
      process.env.APP_SECRET,
      {
        expiresIn: '24h',
      },
    );
    console.log(token);
    const user = await this.update(userUpdate._id, {
      resetPasswordToken: token,
    });
    console.log(user);
    this.sendMailForgotPassword(userUpdate.email, token);

    //return await this.userModel.create(userUpdate);
    return { message: 'Forgot Password: Send Mail successfully!' };
  }

  private sendMailForgotPassword(email, token): void {
    const resetPasswordUrl = `http://localhost:4200/reset-password/${token}`;

    this.mailerService
      .sendMail({
        to: email, 
        subject: 'Reset your password',
        html: `
        <h2>Hello </h2>
        <h3>Please click the following link to reset your password:</h3>
        <h3>Click <a href="${resetPasswordUrl}">here</a> to reset your password!</h3>`,
      }) 
      .then((response) => {
        // response.status ('chek your email!!, send mail successfully')
        console.log(response);
        console.log('Forgot Password: Send Mail successfully!');
      })
      .catch((err) => {
        //err.HttpStatus ('forgetassword : send mail failed')
        console.log(err);
        console.log('Forgot Password: Send Mail Failed!');
      });
  }

  //reset password
  async resetpassword(newpassword: string, token: string): Promise<any> {
    console.log('newpassord', newpassword, typeof newpassword);
    try {
      const resetPasswordToken = token;
      if (resetPasswordToken) {
        console.log('resetpasswordtoken', resetPasswordToken);
        // pour verifier date d'expiration de resetPasswordtoken
        jwt.verify(resetPasswordToken, process.env.APP_SECRET, async (x) => {
          if (x) {
            throw new HttpException('incorrect token ', HttpStatus.NOT_FOUND);
          }
          const decoded = await this.validateToken(resetPasswordToken);
          console.log('decodetoken', decoded);
          const user = await this.userModel.findOne({
            resetPasswordToken: resetPasswordToken,
          });
          console.log('user', user);

          user.password = bcrypt.hashSync(newpassword, 10);
          user.resetPasswordToken = undefined;
          user.save();
          return { message: 'password changée' };
        });
      }
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  async validateToken(reserPasswordToken: string): Promise<any> {
    try {
      const decoded = await jwt.decode(reserPasswordToken);
      return decoded;
    } catch (err) {
      return null;
    }
  }

  //confirmation email
  async verifyEmail(vreified: string): Promise<User> {
    const user = await this.userModel.findOne({ vreified: vreified });
    if (!user) {
      throw new NotFoundException('Invalid verification code');
    }
    user.confirmed = true;
    user.vreified = undefined;
    return user.save();
  }

  //change password
  public async updateByPassword(
    email: string,
    password: string,
  ): Promise<UserDocument> {
    try {
      const user = await this.userModel.findOne({ email: email });
      user.password = bcrypt.hashSync(password, 8);
      if (!user) {
        throw new NotFoundException('Email Not Found');
      }
      // return await this.userModel.save(user);
      return user.save();
    } catch (err) {
      throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }
  }

  public async changePassword(changePasswordDto: CreateUserDto): Promise<any> {
    this.sendMailChangePassword(changePasswordDto);

    return await this.updateByPassword(
      changePasswordDto.email,
      changePasswordDto.password,
    );
  }

  private sendMailChangePassword(user): void {
    this.mailerService
      .sendMail({   
        to: user.email,
        from: 'from@example.com',
        subject: 'Change Password successful ✔',
        text: 'Change Password successful!',
        template: 'index',
        context: {
          title: 'Change Password successful!',
          description:
            'Change Password Successfully! ✔, This is your new password: ' +
            user.password,
          nameUser: user.firstname,
        },
      })
      .then((response) => {
        console.log(response);
        console.log('Change Password: Send Mail successfully!');
      })
      .catch((err) => {
        console.log(err);
        console.log('Change Password: Send Mail Failed!');
      });
  }
  //cofirm formateur
  async confirmerCompteFormateur(id: string): Promise<UserDocument> {
    const formateur = await this.userModel.findById(id);
    formateur.accepted = true;
    return formateur.save();
  }
  //delete formateur avec le test
  //  async deleteFormateurAndTests(id: string): Promise<void> {
  //   const formateur = await this.userModel.findById(id);
  //   if (!formateur) {
  //     throw new Error('Formateur not found');
  //   }

  //   // Supprimer tous les tests associés au formateur
  //   await this.testModel.deleteMany({ formateur: formateur._id });

  //   // Supprimer le formateur
  //   await this.userModel.findByIdAndDelete(id);
  // }

  async getFormateurs() {
    const listformateur = await this.userModel
      .find({ role: 'formateur' })
      // .populate({ path: 'cours', model: 'Cour' })
      // .populate({ path: 'tests', model: 'Test' })

      .exec();
    if (!listformateur || listformateur.length == 0) {
      throw new NotFoundException('Données des formateurs non trouvées !');
    }
    return {
      // message: 'toutes les formateurs trouvées avec succées ',
      total: listformateur.length,
      listformateur,
    };
  }

  async getEtudiants() {
    const listetudiant = await this.userModel.find({ role: 'etudiant' }).exec();
    if (!listetudiant || listetudiant.length == 0) {
      throw new NotFoundException('Données des etudiant non trouvées !');
    }
    return {
      message: 'toutes les etudiants trouvées avec succées ',
      total: listetudiant.length,
      listetudiant,
    };
  }

  async deletFormateur(id: string): Promise<UserDocument> {
    const deleteformatuer = await this.userModel.findByIdAndDelete(id);
    //relation cours
    await this.courModel.findByIdAndUpdate(
      { _id: deleteformatuer.cours },
      { $pull: { id_formateur: deleteformatuer._id } },
    );
    //relation test
    await this.testModel
      .findByIdAndUpdate(
        { _id: deleteformatuer.tests },
        { $pull: { formateur: deleteformatuer._id } },
      )
      .exec(); 
    return deleteformatuer;
  }

 
}
