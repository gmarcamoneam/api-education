import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Res,
  Put,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { AccessTokenGuard } from 'src/common/guards/accessToken.guard';
import { HttpStatus } from '@nestjs/common/enums';


@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get(':id')
  findById(@Param('id') id: string) {
    return this.usersService.findById(id);
  }


   
  @UseGuards(AccessTokenGuard)
  @Put(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id, updateUserDto);
  }

  @UseGuards(AccessTokenGuard)
  @Delete(':id')
  async remove(@Res() response, @Param('id') id: string) {
    try {
      const Delete = await this.usersService.remove(id);
      return response.status(HttpStatus.OK).json({
        message: 'user supprimé avec succès',
        // data: 0,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
  @Post('changepassword')
  public async changePassword(
    @Res() res,
    @Body() changePasswordDto: CreateUserDto,
  ): Promise<any> {
    try {
      await this.usersService.changePassword(changePasswordDto);

      return res.status(HttpStatus.OK).json({
        message: 'Request Change Password Successfully!',
        status: 200,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error: Change password failed!',
        status: 400,
      });
    }
  }
 
  //delete formateur
  @UseGuards(AccessTokenGuard)
  @Delete('deletformateur/:id')
  async deletFormateur(@Res() response, @Param('id') id: string) {
    try {
      const Delete = await this.usersService.deletFormateur(id);
      return response.status(HttpStatus.OK).json({
        message: 'formatuer supprimé avec succès',
        // data: 0,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  
}
