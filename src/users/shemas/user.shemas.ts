import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { CourSchema } from 'src/cours/shemas/cours.shemas';
import { FormationSchema } from 'src/formation/shemas/formation.shemas';
import { TestSchema } from 'src/test/shemas/tests.shemas';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop({ required: true, minlength: 3, maxlength: 20 })
  firstname: string;

  @Prop({ required: true, minlength: 3, maxlength: 20 })
  lastname: string;

  @Prop({ required: true, unique: true })
  email: string;

  @Prop({ required: true, minlength: 3, maxlength: 100 })
  password: string;

  @Prop()
  date_de_naissance: Date;

  @Prop()
  refreshToken: string;

  @Prop()
  picture: string;

  @Prop()
  address: string;

  @Prop()
  vreified: string;

  @Prop({
    type: Boolean,
    // default: false,
  })
  confirmed: Boolean;

  @Prop()
  phone: string;

  @Prop()
  domaine: string; 

  @Prop()
  gender: string;

  @Prop()
  cin: string;

  @Prop()
  resetPasswordToken: string;

  @Prop()
  resetPasswordTokenAt: Date;

  @Prop({
    type: Boolean,
    // default: false,
  })
  accepted: Boolean;

  @Prop({ type: String })
  paiements: [{ type: mongoose.Schema.Types.ObjectId; ref: 'Paiement' }];

  @Prop(CourSchema)
  cours: string;

  @Prop(FormationSchema) //relation entre l'etudiant et le formation l'etudiant choisir une formation
  formation: string;

  @Prop(TestSchema)
  tests: string;

  @Prop({
    type: String,
    enum: ['admin', 'etudiant', 'formateur'],
    // default:'etudiant'
  })
  role: 'admin' | 'etudiant' | 'formateur' = 'etudiant';
}

export const UserSchema = SchemaFactory.createForClass(User);
 